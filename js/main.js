window.addEventListener('load', init);

function init()
{
  getGoogleMaps();
}

function getGoogleMaps(){
  $.ajax({
      url:  'https://maps.googleapis.com/maps/api/js?key=AIzaSyCiIjoKqN6Jl42Rq3GuwmYMECIJ6_zU3ak&libraries=places&callback=initAutocomplete',
      type: 'GET',
      dataType: 'jsonp',
      jsonpCallback: 'callback'

  })  .done(initAutocomplete)
      .fail(failureHandler);
  }

function initAutocomplete() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 51.4536672, lng: 3.570912499999963},
    zoom: 13,
    mapTypeId: 'roadmap'
  });

  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    markers.forEach(function(marker) {
      marker.setMap(null);
    });

    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));


      if (place.geometry.viewport) {
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
    getStores();
    
  });
}



function getStores(){
  $.ajax({
      url:  'https://maps.googleapis.com/maps/api/js?key=AIzaSyCiIjoKqN6Jl42Rq3GuwmYMECIJ6_zU3ak&libraries=places&callback=initMap',
      type: 'GET',
      dataType: 'jsonp',
      jsonpCallback: 'callback'

  })  .done(initMap)
      .fail(failureHandler);
}

function failureHandler(){
  console.log("werkt niet");
}

var map;
var infowindow;

function initMap() {
var vlissingen = {lat: 51.4536672, lng: 3.570912499999963};

map = new google.maps.Map(document.getElementById('map'), {
  center: vlissingen,
  zoom: 13
});

infowindow = new google.maps.InfoWindow();
var service = new google.maps.places.PlacesService(map);
service.nearbySearch({
  location: vlissingen,
  radius: 2000,
  type: ['store']
}, callback);
}

var input = document.getElementById('pac-input');
var searchBox = new google.maps.places.SearchBox(input);
map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

function callback(results, status) {
if (status === google.maps.places.PlacesServiceStatus.OK) {
  for (var i = 0; i < results.length; i++) {
    createMarker(results[i]);
  }
}
}

function createMarker(place) {
var placeLoc = place.geometry.location;
var marker = new google.maps.Marker({
  map: map,
  position: place.geometry.location
});

google.maps.event.addListener(marker, 'click', function() {
  infowindow.setContent(place.name);
  infowindow.open(map, this);
});
}



